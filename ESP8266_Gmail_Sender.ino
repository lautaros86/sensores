#include <ESP8266WiFi.h>
#include "Gsender.h"
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
ESP8266WebServer server(82);

#pragma region Globals
const char* ssid = "SABA";                           // WIFI network name
const char* password = "1142487174";                       // WIFI network password
uint8_t connection_state = 0;                    // Connected to WIFI or not
uint16_t reconnect_interval = 10000;             // If not connected wait time to try again


#pragma endregion Globals

boolean AlarmaActivada;
boolean Act_Sensor_1;
boolean Act_Sensor_2;
boolean Act_Sensor_3;
boolean Act_Sensor_4;
boolean Act_Sensor_5;

boolean Act_C_Remoto1_1;
boolean Act_C_Remoto1_2;
boolean Act_220V;
boolean Act_Salida_Sonora;
//boolean Act_Salida_Alarma_Activada;
boolean Act_Intrusion_2;

boolean Inhibit_Sensor_1;
boolean Inhibit_Sensor_2;
boolean Inhibit_Sensor_3;
boolean Inhibit_Sensor_4;
boolean Inhibit_Sensor_5;

boolean Inhibit_C_Remoto1_1;
boolean Inhibit_C_Remoto1_2;
boolean Inhibit_220V;
boolean Inhibit_Salida_Sonora;
boolean Inhibit_Salida_Alarma_Activada;

boolean Inhibit_Intrusion_1;
boolean Inhibit_Intrusion_2;
int TimerSirena;
String HTMLpage = "";
float VoltBat;
boolean EmailVoltBatBajoEstado=false;
boolean EmailVoltBatNormalEstado=false;

void MostrarHTTP()
{
  HTMLpage = "<head><title>Alarma General</title></head><h3>MENU PRINCIPAL ALARMA (V1.0)</h3>";
  Serial.println(AlarmaActivada);
  Serial.println("Mostrar HTTP");

  if (AlarmaActivada == true)
  { HTMLpage += "<p>ESTADO DE ALARMA: ALARMA ACTIVADA";
    Serial.println("If AlarmaActivada=true");
    Serial.println(AlarmaActivada);
  }
  else
  {
    HTMLpage += "<p>ESTADO DE ALARMA: ALARMA DESACTIVADA";
    Serial.println("else =0 AlarmaActivada=false");
    Serial.println(AlarmaActivada);
  }

  HTMLpage += "<p>ESTADO DE ALARMA (ACTIVADO/DESACTIVADA): <a href=\"A_ACTIVAR_ALARMA\"><button>ACTIVAR</button></a>&nbsp;<a href=\"A_DESACTIVAR\"><button>DESACTIVAR</button></a></p>";

  if (Act_Salida_Sonora == true)
    HTMLpage += "<p>SALIDA SONORA: ACTIVADA / INTRUSION!!!";
  else
    HTMLpage += "<p>SALIDA SONORA: DESACTIVADA / NO EXISTE INTRUSION";

  HTMLpage += "</title></head><h3>ESTADO DE SENSORES</h3>";

  if (Inhibit_Sensor_1 == true)
    HTMLpage += "<p>ESTADO DE SENSOR #1: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SENSOR #1: DESACTIVADO";

  HTMLpage += "<p>SENSOR _1 <a href=\"ACTIVAR_S_1\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_S_1\"><button>DESACTIVAR</button></a></p> </h3>";

  if (Inhibit_Sensor_2 == true)
    HTMLpage += "<p>ESTADO DE SENSOR #2: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SENSOR #2: DESACTIVADO";

  HTMLpage += "<p>SENSOR _2 <a href=\"ACTIVAR_S_2\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_S_2\"><button>DESACTIVAR</button></a></p> </h3>";

  if (Inhibit_Sensor_3 == true)
    HTMLpage += "<p>ESTADO DE SENSOR #3: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SENSOR #3: DESACTIVADO";

  HTMLpage += "<p>SENSOR _3 <a href=\"ACTIVAR_S_3\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_S_3\"><button>DESACTIVAR</button></a></p> </h3>";

  if (Inhibit_Sensor_4 == true)
    HTMLpage += "<p>ESTADO DE SENSOR #4: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SENSOR #4: DESACTIVADO";

  HTMLpage += "<p>SENSOR _4 <a href=\"ACTIVAR_S_4\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_S_4\"><button>DESACTIVAR</button></a></p> </h3>";

  if (Inhibit_Sensor_5 == true)
    HTMLpage += "<p>ESTADO DE SENSOR #5: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SENSOR #5: DESACTIVADO";

  HTMLpage += "<p>SENSOR _5 <a href=\"ACTIVAR_S_5\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_S_5\"><button>DESACTIVAR</button></a></p> </h3>";

  if (Inhibit_Salida_Sonora == true)
    HTMLpage += "<p>ESTADO DE SALIDA #1: ACTIVADO";
  else
    HTMLpage += "<p>ESTADO DE SALIDA #1: DESACTIVADO";

  HTMLpage += "<p>SALIDA _1 <a href=\"ACTIVAR_SAL_1\"><button>ACTIVAR</button></a>&nbsp;<a href=\"DESACTIVAR_SAL_1\"><button>DESACTIVAR</button></a></p> </h3>";
  HTMLpage += "<p>ESTADO DE BATERIA [Volt]: ";
  HTMLpage += VoltBat;
  
   HTMLpage += "<p>BATERIA <a href=\"ACTIVAR_S_1\"><button>ACTIVAR_BAT</button></a>&nbsp;<a href=\"DESACTIVAR_BAT\"><button>DESACTIVAR</button></a></p> </h3>";

  server.send(200, "text/html", HTMLpage);

}
void VerificarEstado()
{
  //float VoltBat;
  float analogPin = 0;
  Serial.println("Verificando Estado");
  Act_Sensor_1 = digitalRead(16);
  Act_Sensor_2 = digitalRead (5);
  Act_Sensor_3 = digitalRead (4);
  Act_Sensor_4 = digitalRead (14);
  Act_Sensor_5 = digitalRead (12);
  Act_C_Remoto1_1 = digitalRead (0);
  Act_C_Remoto1_2 = digitalRead (2);
  analogPin  = analogRead (0);
  VoltBat = analogPin / 70;

  Serial.println("Sensores 1-5");
  Serial.println(Act_Sensor_1);
  Serial.println(Act_Sensor_2);
  Serial.println(Act_Sensor_3);
  Serial.println(Act_Sensor_4);
  Serial.println(Act_Sensor_5);
  Serial.println("Inibicion de sensores 1-5");
  Serial.println(Inhibit_Sensor_1);
  Serial.println(Inhibit_Sensor_2);
  Serial.println(Inhibit_Sensor_3);
  Serial.println(Inhibit_Sensor_4);
  Serial.println(Inhibit_Sensor_5);
  //---------------------------------------Invierte la logica de las entradas del control remoto ya que estan normalmente en 1
  if (Act_C_Remoto1_1 == true )
  {
    Act_C_Remoto1_1 = false;
  }
  else
  { Act_C_Remoto1_1 = true;
  }

  if (Act_C_Remoto1_2 == true )
  {
    Act_C_Remoto1_2 = false;
  }
  else
  { Act_C_Remoto1_2 = true;
  }

  //----------------------------------------------------------------------------------------------------------------------------


  Serial.println("C_Remoto (B-D0)GPIO0/D2,(A-D3)GPIO2, BAT(0-255), BATERIA (VOLT)");
  Serial.println(Act_C_Remoto1_1);
  Serial.println(Act_C_Remoto1_2);
  Serial.println (VoltBat);

  if (Act_C_Remoto1_1 == true && AlarmaActivada == false)
  {
    A_ACTIVAR_ALARMAF();
    Act_C_Remoto1_1 = false;
  }
  if (Act_C_Remoto1_1 == true && AlarmaActivada == true)
  {
    A_DESACTIVARF();
    Act_C_Remoto1_1 = false;
  }

  // ACTIVAR PANICO----------------------------------------------------------
  if (Act_C_Remoto1_2 == true && Act_Salida_Sonora == false)
  {
    Act_Salida_Sonora = true;
    Act_C_Remoto1_2 = false;
    digitalWrite (13, HIGH);;

  }
  if (Act_C_Remoto1_2 == true && Act_Salida_Sonora == true)
  {
    digitalWrite (13, LOW);
    Act_Salida_Sonora = false;
    Act_C_Remoto1_2 = false;
  }
  //--------------------------------------------------------------------------

  Serial.println("Alarma Activada,Salida Sonora");
  Serial.println(AlarmaActivada);
  Serial.println(Act_Salida_Sonora);
  delay (300);

  if (AlarmaActivada == true && Act_Salida_Sonora == false)
  {
    Serial.println("Alarma Activada");
    if ((!Act_Sensor_1 && Inhibit_Sensor_1) || (!Act_Sensor_2 && Inhibit_Sensor_2) || (!Act_Sensor_3 && Inhibit_Sensor_3) || (!Act_Sensor_4 && Inhibit_Sensor_4) || (!Act_Sensor_5 && Inhibit_Sensor_5))
    {
      Serial.println("Alarma Activada y algun sensor detecto presencia: SonarSirena");
      Act_Salida_Sonora = true;
    }
    ActivarSirena();
  }

 if ( VoltBat<10 && EmailVoltBatBajoEstado==false)
    {
     EmailVoltBatBajo();
     EmailVoltBatBajoEstado=true;
    }
 if (VoltBat>12 && EmailVoltBatBajoEstado==true && EmailVoltBatNormalEstado==false)
 {
     EmailVoltBatNormal();
     EmailVoltBatNormalEstado=true;
     EmailVoltBatBajoEstado=false;
    }


  
}

//-------------------------------------------------------

void EmailVoltBatBajo()
  {
    String EstadoAEnviar;
    
     Gsender *gsender = Gsender::Instance();    // Getting pointer to class instance
      String subject = "Bateria menor a 10 Volt";

      EstadoAEnviar= "Sensor 1:";
      EstadoAEnviar += Act_Sensor_1 ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar += "\n\r Sensor 2:";
      EstadoAEnviar += Act_Sensor_2 ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar += "\n\r Sensor 3:";
      EstadoAEnviar += Act_Sensor_3 ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar += "\n\r Sensor 4:";
      EstadoAEnviar += Act_Sensor_4 ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar += "\n\r Sensor 5:";
      EstadoAEnviar += Act_Sensor_5 ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar += "\n\r Bateria:";
      EstadoAEnviar += VoltBat ;
      EstadoAEnviar += "Volt" ;
      EstadoAEnviar= "AlarmaActivada:";
      EstadoAEnviar += AlarmaActivada ;
      EstadoAEnviar += "\n\r" ;
      EstadoAEnviar= "Intrusion:";
      EstadoAEnviar += Act_Salida_Sonora;
      EstadoAEnviar += "\n\r" ;
  
      if (gsender->Subject(subject)->Send("gustavo.tedeschi@gmail.com", EstadoAEnviar))
      {
        Serial.println("Message send.");
      } else
      {
        Serial.print("Error sending message: ");
        Serial.println(gsender->getError());
      }
  }

void EmailVoltBatNormal()
  {
     Gsender *gsender = Gsender::Instance();    // Getting pointer to class instance
     String subject = "Bateria mayor a 12 Volt";
      
      if (gsender->Subject(subject)->Send("gustavo.tedeschi@gmail.com", "Bateria mayor a 12 Volt  "))
      {
        Serial.println("Message send.");
      } else
      {
        Serial.print("Error sending message: ");
        Serial.println(gsender->getError());
      }
  }
    

void ActivarSirena()
{
  if (TimerSirena <= 30 && Act_Salida_Sonora == true && AlarmaActivada==true  )
  {
    Act_Salida_Sonora = true;
    digitalWrite (13, HIGH);
    delay (1000);

    // --Enviar email
    if (TimerSirena == 0)
    {
      Gsender *gsender = Gsender::Instance();    // Getting pointer to class instance
      String subject = "Alarma de intruso";
      if (gsender->Subject(subject)->Send("gustavo.tedeschi@gmail.com", "Alarma de intruso"))
      {
        Serial.println("Message send.");
      } else
      {
        Serial.print("Error sending message: ");
        Serial.println(gsender->getError());

      }
    }
    //--fin enviar email
    TimerSirena++;
    Serial.println("Act_Salida_Sonora=true");
    Serial.println("Timer:");
    Serial.println(TimerSirena);

  }
  else
  {
    Act_Salida_Sonora = false;
    TimerSirena = 0;
    digitalWrite (13, LOW);
    Serial.println("Else de Activar alarma");
    Serial.println("Timer:");
    Serial.println(TimerSirena);
  }
}


void A_ACTIVAR_ALARMAF()

{
  AlarmaActivada = true;
  digitalWrite (15, HIGH); //
  MostrarHTTP();
}

void A_DESACTIVARF()
{
  AlarmaActivada = false;
  digitalWrite (15, LOW);

}

void A_ACTIVAR_S_1F()
{
  Inhibit_Sensor_1 = 1;
  MostrarHTTP();
}

void DESACTIVAR_S_1F()
{
  Inhibit_Sensor_1 = 0;
  MostrarHTTP();
}

void DESACTIVAR_S_2F()
{
  Inhibit_Sensor_2 = 0;
  MostrarHTTP();
}
void A_ACTIVAR_S_2F()
{
  Inhibit_Sensor_2 = 1;
  MostrarHTTP();
}

void DESACTIVAR_S_3F()
{
  Inhibit_Sensor_3 = 0;
  MostrarHTTP();
}
void A_ACTIVAR_S_3F()
{
  Inhibit_Sensor_3 = 1;
  MostrarHTTP();
}

void DESACTIVAR_S_4F()
{
  Inhibit_Sensor_4 = 0;
  MostrarHTTP();
}
void A_ACTIVAR_S_4F()
{
  Inhibit_Sensor_4 = 1;
  MostrarHTTP();
}

void DESACTIVAR_S_5F()
{
  Inhibit_Sensor_5 = 0;
  MostrarHTTP();
}
void A_ACTIVAR_S_5F()
{
  Inhibit_Sensor_5 = 1;
  MostrarHTTP();
}


void ACTIVAR_SAL_1F()
{
  Inhibit_Salida_Sonora = 1;
  MostrarHTTP();
}

void DESACTIVAR_SAL_1F()
{
  Inhibit_Salida_Sonora = 0;
  MostrarHTTP();
}

void ACTIVAR_INT_1F()
{
  Inhibit_Intrusion_1 = 1;
  MostrarHTTP();
}

void DESACTIVAR_INT_1F()
{
  Inhibit_Intrusion_1 = 0;
  MostrarHTTP();
}

void ACTIVAR_INT_2F()
{
  Inhibit_Intrusion_2 = 1;
  MostrarHTTP();
}

void DESACTIVAR_INT_2F()
{
  Inhibit_Intrusion_2 = 0;
  MostrarHTTP();
}
void handle_root()
{
  MostrarHTTP();

}

uint8_t WiFiConnect(const char* nSSID = nullptr, const char* nPassword = nullptr)
{
  static uint16_t attempt = 0;
  Serial.print("Connecting to ");
  if (nSSID) {
    WiFi.begin(nSSID, nPassword);
    Serial.println(nSSID);
  } else {
    WiFi.begin(ssid, password);
    Serial.println(ssid);
  }

  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED && i++ < 50)
  {
    delay(200);
    Serial.print(".");
  }
  ++attempt;
  Serial.println("");
  if (i == 51) {
    Serial.print("Connection: TIMEOUT on attempt: ");
    Serial.println(attempt);
    if (attempt % 2 == 0)
      Serial.println("Check if access point available or SSID and Password\r\n");
    return false;
  }
  Serial.println("Connection: ESTABLISHED");
  Serial.print("Got IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Test Salida GPIO0/D3");

  for (int i = 0; i < 3;) // 'i' solo es visible
  {
    Serial.println(i);
    i++;
    digitalWrite (15, HIGH);
    digitalWrite (13, LOW);
    delay (200);
    digitalWrite (15, LOW);
    digitalWrite (13, HIGH);
    delay (400);
  }
  digitalWrite (13, LOW);

}

void Awaits()
{
  uint32_t ts = millis();
  while (!connection_state)
  {
    delay(50);
    if (millis() > (ts + reconnect_interval) && !connection_state) {
      connection_state = WiFiConnect();
      ts = millis();
    }
  }
}

void setup(void)
{
  pinMode (16, INPUT);      //  D0 - Sensor_1
  pinMode (5, INPUT);       //  D1 - Sensor_2
  pinMode (4, INPUT);       //  D2 - Sensor_3
  pinMode (14, INPUT);      //  D5 - Sensor_4
  pinMode (12, INPUT);      //  D6 - Sensor_5
  pinMode (1, INPUT);       //  TX - Sensor_220V

  // pinMode (0, OUTPUT);       //  D3 - CONTROL (D0)
  // digitalWrite (0, LOW);

  pinMode (0, INPUT);       //D3 - CONTROL (D0)

  //pinMode (2, OUTPUT);       //  D3 - CONTROL (D0)
  //digitalWrite (2, LOW);
  pinMode (2, INPUT);       //  D3 - CONTROL (D0)
  pinMode (2, INPUT);       //  D4 - CONTROL (D2)

  pinMode (13, OUTPUT);     //  D7 -RX Salida Sonora
  pinMode (15, OUTPUT);     //  D8 - Alarma Activada/Desactivada


  Serial.begin(115200);


  connection_state = WiFiConnect();
  if (!connection_state) // if not connected to WIFI
    Awaits();          // constantly trying to connect
  //-----------------

  server.on("/", handle_root);

  server.on("/A_ACTIVAR_ALARMA", A_ACTIVAR_ALARMAF);
  server.on("/A_DESACTIVAR", A_DESACTIVARF);

  server.on("/DESACTIVAR_S_1", DESACTIVAR_S_1F);
  server.on("/ACTIVAR_S_1", A_ACTIVAR_S_1F);

  server.on("/DESACTIVAR_S_2", DESACTIVAR_S_2F);
  server.on("/ACTIVAR_S_2", A_ACTIVAR_S_2F);

  server.on("/DESACTIVAR_S_3", DESACTIVAR_S_3F);
  server.on("/ACTIVAR_S_3", A_ACTIVAR_S_3F);

  server.on("/DESACTIVAR_S_4", DESACTIVAR_S_4F);
  server.on("/ACTIVAR_S_4", A_ACTIVAR_S_4F);

  server.on("/DESACTIVAR_S_5", DESACTIVAR_S_5F);
  server.on("/ACTIVAR_S_5", A_ACTIVAR_S_5F);

  server.on("/DESACTIVAR_SAL_1", DESACTIVAR_SAL_1F);
  server.on("/ACTIVAR_SAL_1", ACTIVAR_SAL_1F);

  server.on("/DESACTIVAR_INT_1", DESACTIVAR_INT_1F);
  server.on("/ACTIVAR_INT_1", ACTIVAR_INT_1F);

  server.on("/DESACTIVAR_INT_2", DESACTIVAR_INT_2F);
  server.on("/ACTIVAR_INT_2", ACTIVAR_INT_2F);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.begin();
  Serial.println("HTTP server started");

}



void loop(void)
{
  server.handleClient();
  VerificarEstado();
  ActivarSirena();


  //----------------

  //----------------------
}
